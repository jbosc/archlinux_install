#! /bin/bash
mkdir -p /mnt/btrfs-root
mount -o defaults,relatime,space_cache /dev/mapper/root /mnt/btrfs-root
mkdir -p /mnt/btrfs-root/__active
mkdir -p /mnt/btrfs-root/__snapshot

cd /mnt/btrfs-root
btrfs subvolume create __active/rootvol
btrfs subvolume create __active/home
btrfs subvolume create __active/var
btrfs subvolume create __active/opt

# 7 - Create mountpoints and mount the btrfs subvolumes on the correct hierarchy
mkdir -p /mnt/btrfs-active
mount -o defaults,nodev,relatime,space_cache,subvol=__active/rootvol /dev/mapper/root /mnt/btrfs-active
# create the mountpoints and mount separately /home, /opt, /var and /var/lib
mkdir -p /mnt/btrfs-active/{home,opt,var,var/lib,boot}
mount -o defaults,nosuid,nodev,relatime,subvol=__active/home /dev/mapper/root /mnt/btrfs-active/home
mount -o defaults,nosuid,nodev,relatime,subvol=__active/opt /dev/mapper/root /mnt/btrfs-active/opt
mount -o defaults,nosuid,nodev,noexec,relatime,subvol=__active/var /dev/mapper/root /mnt/btrfs-active/var
# /var/lib is special, since it's very useful for snapshots of it to be part of the active root volume.
# To manage that, we bind-mount the directory from the "rootvol" subvolume back inside the var subvolume
mkdir -p /mnt/btrfs-active/var/lib
mount --bind /mnt/btrfs-root/__active/rootvol/var/lib /mnt/btrfs-active/var/lib
# you need to make sure that this directory exists, there's a step below for that

# 8 - Mount the EFI partition
# we're using /boot here, not /boot/efi as some suggest, since our / is encrypted.
# apparently, grub2 can manage this, but I haven't been able to replicate it.
mount -o defaults,nosuid,nodev,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,errors=remount-ro /dev/sda1 /mnt/btrfs-active/boot
